<?php

namespace App;

/**
 * Class StrSplitter
 *
 * Trial assignment
 *
 * @package App
 */
class StrSplitter
{
    /**
     * @param string $str
     * @param int $num
     * @param bool $firstLoop
     * @return array
     */
    public function getStringVariationsByNum(string $str, int $num, bool $firstLoop = true)
    {
        $result = [];

        // Saving original string;
        if ($firstLoop) {
            $result[0] = [$str];
        }

        for ($i = $num; $i <= strlen($str) - $num; $i++) {
            $str_start = substr($str, 0, $i);
            $str_end = substr($str, $i);

            $result[] = [$str_start, $str_end];
            foreach ($this->getStringVariationsByNum($str_end, $num, false) as $arr) {
                array_unshift($arr, $str_start);
                $result[] = $arr;
            }
        }

        return $result;
    }
}