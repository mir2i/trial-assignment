<?php

namespace App\Tests;

use App\StrSplitter;
use PHPUnit\Framework\TestCase;


class StrSplitterShould extends TestCase
{
    /** @var \App\StrSplitter */
    protected $strSplitter;

    protected function setUp()
    {
        $this->strSplitter = new StrSplitter();
    }

    /** @test
     * @dataProvider getData
     */
    public function splitString(string $str, int $num, array $expected)
    {
        $result = $this->strSplitter->getStringVariationsByNum($str, $num);

        // Sort arrays for fair comparison
        array_multisort($result, SORT_ASC);
        array_multisort($expected, SORT_ASC);

        // Check
        $this->assertEquals($expected, $result);
    }

    public function getData()
    {
        return [
            ['qwertyuiolk', 3, [
                ['qwe', 'rty', 'uiolk'],
                ['qwe', 'rtyu', 'iolk'],
                ['qwe', 'rtyui', 'olk'],
                ['qwe', 'rtyuiolk'],
                ['qwer', 'tyu', 'iolk'],
                ['qwer', 'tyui', 'olk'],
                ['qwer', 'tyuiolk'],
                ['qwert', 'yui', 'olk'],
                ['qwert', 'yuiolk'],
                ['qwerty', 'uiolk'],
                ['qwertyu', 'iolk'],
                ['qwertyui', 'olk'],
                ['qwertyuiolk']
            ]
            ],
            ['qwertyuiolk', 5, [
                ['qwert', 'yuiolk'],
                ['qwerty', 'uiolk'],
                ['qwertyuiolk']
            ]
            ],
            ['abcde', 2, [
                ['ab', 'cde'],
                ['abc', 'de'],
                ['abcde']
            ]
            ],
            ['catdog', 3, [
                ['cat', 'dog'],
                ['catdog']
            ]
            ]
        ];
    }
}
